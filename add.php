<?php

if(!isset($_SESSION)){
    session_start();
}

include_once('connections/connection.php');
$con = connection();

if (isset($_POST['submit'])) {

    $fname = $_POST['first_name'];
    $lname = $_POST['last_name'];
    $gender = $_POST['gender'];
    $bday = $_POST['birth_day'];

    $sql = "INSERT INTO `student_list`(`first_name`, `last_name`,`gender`, `birth_day`) VALUES ('$fname','$lname','$gender', '$bday')";
    $con->query($sql) or die ($con->error);

    echo header('Location: index.php');
}


?>


<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Student Kit</title>
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css">
</head>
<body>

    <nav class="navbar navbar-dark fixed-top bg-dark">

        <div class="nav-logo">
            <a href="#">Student Management System</a>
        </div>

        <!-- welcome -->
        <?php
        if(isset($_SESSION['UserLogin'])) { ?>
        <?php echo "Welcome ".$_SESSION['UserLogin']; ?>
        <?php } else { ?>
        <?php echo "Welcome Guest"; ?>
        <?php } ?>
        <!-- end welcome -->

        <!-- login/logout -->
        <?php
        if(isset($_SESSION['UserLogin'])) { ?>
            <button class="btn btn-outline-danger logout"><a href="logout.php">Log-out</a></button>
        <?php } else { ?>
            <button class="btn btn-outline-info loginsuccess"><a href="login.php">Log-in</a></button>
        <?php } ?>
        <!-- end login/logout -->

    </nav> 

<?php if(isset($_SESSION['UserLogin'])) { ?>
<br><br><br><br>


<div class="add-container">
    
    <form action="" method="post">

        <label for="First Name">First Name:</label>
            <input type="text" name="first_name" id="first_name" class="form-control" required>
        
        <label for="Last Name">Last Name:</label>
            <input type="text" name="last_name" id="last_name" class="form-control" required> 

        <label for="Gender">Gender: </label>
            <select name="gender" id="gender" class="form-control">
                <option value="Male">Male</option>
                <option value="Female">Female</option>
            </select>
        <label for="Birthday">Birthday: </label>
            <input type="date" name="birth_day" id="birth_day" class="form-control" required>
        <br><br>
        
        <div class="d-flex">
            <div class="col-sm-6 d-flex">
                <a href="javascript:history.back()" class="btn btn-dark">Go Back</a>
            </div>
            <div class="col-sm-6 d-flex justify-content-end">
                <input class="btn btn-primary" type="submit" name="submit" value="Submit">
            </div> 
        </div>
    </form>
</div>
<?php } else { ?>
    <?php echo "<br><br><h1>Please log-in first.</h1>"; ?>
    <?php } ?>
</body>
</html>

