<?php

if(!isset($_SESSION)){

    session_start();

}

if(isset($_SESSION['UserLogin'])) {
    echo "Welcome ".$_SESSION['UserLogin'];

} else {
    echo "Welcome Guest";
}

include_once('connections/connection.php');

$con = connection();

$search = $_GET['search'];

$sql = "SELECT * FROM student_list WHERE first_name LIKE '%$search%' || last_name LIKE '%$search%' || gender = '$search' ORDER BY id DESC";
$students = $con->query($sql) or die ($con->error);
$row = $students->fetch_assoc();

// do {
//     echo $row['first_name'].' '.$row['last_name']. '<br/>';
// }while($row = $students->fetch_assoc());
?>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Student Kit</title>
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css">
</head>
<body>


    <nav class="navbar navbar-dark fixed-top bg-dark">
        <div class="nav-logo">
            <a href="#">Student Management System</a>
        </div>

        <!-- welcome -->
        <?php
        if(isset($_SESSION['UserLogin'])) { ?>
        <?php echo "Welcome ".$_SESSION['UserLogin']; ?>
        <?php } else { ?>
        <?php echo "Welcome Guest"; ?>
        <?php } ?>
        <!-- end welcome -->
        <!-- login/logout -->
        <?php
        if(isset($_SESSION['UserLogin'])) { ?>
            <button class="btn btn-outline-danger logout"><a href="logout.php">Log-out</a></button>
        <?php } else { ?>
            <button class=" btn-info loginsuccess"><a href="login.php">Log-in</a></button>
        <?php } ?>
        <!-- end login/logout -->
    </nav>
<?php if(isset($_SESSION['UserLogin'])) { ?>          

<!-- add search    -->
<div class="container index-container">
    <!-- add -->
    <button class="btn btn-outline-success add">
        <a href="add.php">Add New</a>
    </button>
    <!-- search -->
    <form class="form-inline" action="result.php" method="GET">
        <h1>Student's Table List</h1>
        <div class="col-sm-12 d-flex justify-content-end">
            <label for="Search"></label>
            <input type="text" name="search" id="search" class="form-control">
                <button class="btn btn-search btn-primary" type="submit">Search</button>
                <button class="btn btn-dark reset"><a href="index.php">Reset</a></button>
        </div>
    </form>

<table class="table1">
<thead>
    <tr>
        <th>First Name</th>
        <th>Last Name</th>
        <th>Gender</th>
        <th>Birthday</th>
        <th>Action</th>
    </tr>
</thead>

<tbody>
    <?php do { ?>

    <tr>
        <td> <?php echo $row['first_name'] ?? ''; ?> </td>
        <td> <?php echo $row['last_name'] ?? ''; ?> </td>
        <td> <?php echo $row['gender'] ?? ''; ?> </td>
        <td> <?php echo $row['birth_day'] ?? ''; ?> </td>
        <td>
            <button class="btn btn-info vw"><a href=" details.php?ID=<?php echo $row['id'];?> ">View</a></button>
            <?php if($_SESSION['Access'] == "administrator") { ?>
                <!-- edit -->
                <button class="btn btn-primary edt"><a href=" edit.php?ID=<?php echo $row['id'];?> ">Edit</a></button>
                
                <!-- delete -->
                <form action="delete.php" method="POST">
                <button class="btn btn-danger btn-delete del" type="submit" name="delete">Delete</button>
                            <input type="hidden" name="ID" value="<?php echo $row['id']; ?>" hidden>
                </form>
            <?php } ?>
        </td>
    </tr>
    <?php } while ($row = $students->fetch_assoc()) ?>
    
<tbody>

</table>
</div>

<?php } else { ?>
<?php echo "<br><br><h1>Please log-in first.</h1>"; ?>
<?php } ?>



</body>
</html>

