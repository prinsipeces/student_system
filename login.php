<?php

if(!isset($_SESSION)){
    session_start();
}


include_once('connections/connection.php');
$con = connection();

if(isset($_POST["login"])) {  
    if(empty($_POST["username"]) || empty($_POST["password"])) {  
        echo '<script>alert("Both fields are required. Please try again")</script>';  
    } else {  
    $username = mysqli_real_escape_string($con, $_POST["username"]);  
    $password = mysqli_real_escape_string($con, $_POST["password"]);  
    $query = "SELECT * FROM student_users WHERE username = '$username'";  
    
    $result = mysqli_query($con, $query);

    if(mysqli_num_rows($result) > 0) {  
            while($row = mysqli_fetch_array($result)) {  
                if(password_verify($password, $row["password"])) {  
                    //return true;  
                    $_SESSION['UserLogin'] = $row['username'];
                    $_SESSION['Access'] = $row['access'];
                    echo header("Location: index.php");  
                } else {  
                    //return false;  
                    echo "<script>alert('Username/Password is incorrect. Please try again.')</script>";  
                }  
            }
        }
        else {  
            //return false;  
            echo "<script>alert('Username/Password is incorrect. Please try again.')</script>";  
        }     
    }  
}  

?>


<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Student Kit</title>
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css">

</head>
<body>

    <div class="container1">
        <h2>Login Page</h2>
        <br>

        <form action="" method="post">
            <div class="form-group">
                <label class="label1" for="username">Username</label>
                <input type="text" name="username" id="username" class="form-control">
            </div>
            <div class="form-group">
                <label class="label1" for="password">Password</label>
                <input type="password" name="password" id="password" class="form-control">
            </div>
          
            <button type="submit" class="btn btn-primary" name="login">Login</button>
           
        </form>
    </div> 


</body>
</html>

