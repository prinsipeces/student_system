<?php

if(!isset($_SESSION)){
    session_start();
}

include_once('connections/connection.php');
$con = connection();

if (isset($_POST['submit'])) {

    $username = $_POST['username'];
    $password = $_POST['password'];
    $password = password_hash($password, PASSWORD_DEFAULT);
    $access = $_POST['access'];

    $sql = "INSERT INTO `student_users`(`username`, `password`, `access`) values('$username', '$password', '$access')";
    $con->query($sql) or die($con->error);

    echo header("Location: index.php");

}

?>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Add New User</title>
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css">
</head>
<body>
    <nav class="navbar navbar-dark fixed-top bg-dark">
        <div class="nav-logo">
            <a href="#">Student Management System</a>
        </div>
        <!-- welcome -->
        <?php
        if(isset($_SESSION['UserLogin'])) { ?>
        <?php echo "Welcome ".$_SESSION['UserLogin']; ?>
        <?php } else { ?>
        <?php echo "Welcome Guest"; ?>
        <?php } ?>
        <!-- end welcome -->
        <!-- login/logout -->
        <?php
        if(isset($_SESSION['UserLogin'])) { ?>
            <button class="btn btn-outline-danger logout"><a href="logout.php">Log-out</a></button>
        <?php } else { ?>
            <button class="btn btn-outline-info loginsuccess"><a href="login.php">Log-in</a></button>
        <?php } ?>
        <!-- end login/logout -->
    </nav>
<?php if(isset($_SESSION['UserLogin'])) { ?>
    <br><br><br><br>

    <div class="newuser-container">
        <form action="" method="post">
            <label for="Username">Username: </label>
            <input type="text" name="username" class="form-control" required>

            <label for="Password">Password: </label>
            <input type="text" name="password" class="form-control" required>
            <br>
            <label for="Access">Access: </label>
            <select name="access" class="form-control" required>
                <option></option>
                <option value="administrator">Administrator</option>
                <option value="teacher">Teacher</option>
            </select>
            <br><br>
            <div class="d-flex">
            <div class="col-sm-6 d-flex">
                <a href="javascript:history.back()" class="btn btn-dark">Go Back</a>
            </div>
            <div class="col-sm-6 d-flex justify-content-end">
                <input class="btn btn-primary" type="submit" name="submit" value="Submit">
            </div> 
        </div>
        </form>
    </div>
<?php } else { ?>
<?php echo "<br><br><h1>Please log-in first.</h1>"; ?>
<?php } ?>

</body>
</html>