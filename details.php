<?php

if(!isset($_SESSION)){

    session_start();

}



include_once('connections/connection.php');

$con = connection();


$id = $_GET['ID'];


$sql = "SELECT * FROM student_list WHERE id ='$id' ";
$students = $con->query($sql) or die ($con->error);
$row = $students->fetch_assoc();

// do {
//     echo $row['first_name'].' '.$row['last_name']. '<br/>';
// }while($row = $students->fetch_assoc());
?>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Student Kit</title>
    <link rel="stylesheet" href="css/style.css">
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css">
</head>
<body>

<nav class="navbar navbar-dark fixed-top bg-dark">

        <div class="nav-logo">
            <a href="#">Student Management System</a>
        </div>

    <!-- welcome -->
        <?php
        if(isset($_SESSION['UserLogin'])) { ?>
        <?php echo "Welcome ".$_SESSION['UserLogin']; ?>
        <?php } else { ?>
        <?php echo "Welcome Guest"; ?>
        <?php } ?>
    <!-- end welcome -->

    <!-- login/logout -->
        <?php
        if(isset($_SESSION['UserLogin'])) { ?>
            <button class="btn btn-outline-danger logout"><a href="logout.php">Log-out</a></button>
        <?php } else { ?>
            <button class="btn btn-outline-info loginsuccess"><a href="login.php">Log-in</a></button>
        <?php } ?>
    <!-- end login/logout -->

    </nav>
    <?php if(isset($_SESSION['UserLogin'])) { ?>     

    <div class="details-container">
        <h2><?php echo $row['first_name']; ?> <?php echo $row['last_name']; ?> is a <?php echo $row['gender']; ?></h2>
        <h2>Born on <?php echo $row['birth_day']; ?></h2>

        <?php if($_SESSION['Access'] == "administrator") { ?>
         
            <div class="d-flex">
                <div class="col-sm-6 d-flex">
                    <a href="javascript:history.back()" class="btn btn-dark">Go Back</a>
                </div>
                <div class="col-sm-6 d-flex justify-content-end">
                    <a href="edit.php?ID=<?php echo $row['id']; ?>" class="btn btn-primary">Edit</a>
                </div> 
            </div>
            
        <?php } else { ?>
            <div class="col-sm-6 d-flex">
                <a href="javascript:history.back()" class="btn btn-dark">Go Back</a>
            </div>
        <?php } ?>

    </div>
    
<?php } else { ?>
<?php echo "<br><br><h1>Please log-in first.</h1>"; ?>
<?php } ?>

</body>
</html>

